VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Rutinas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
 'CODIGO PARA BOOTEAR EL EQUIPO
Private Type LARGE_INTEGER 'LUID
    UsedPart As Long
    IgnopredForNowHigh32bitPart As Long
End Type
             
Private Type TOKEN_PRIVILEGES
    PrivilegeCount As Long
    TheLuid As LARGE_INTEGER
    Attributes As Long
End Type

Public Enum Alienacion
    flexDefault = -1
    flexAlignLeftTop = 0
    flexAlignLeftCenter = 1
    flexAlignLeftBottom = 2
    flexAlignCenterTop = 3
    flexAlignCenterCenter = 4
    flexAlignCenterBottom = 5
    flexAlignRightTop = 6
    flexAlignRightCenter = 7
    flexAlignRightBottom = 8
    flexAlignGeneral = 9
End Enum

Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Sub GetSystemInfo Lib "kernel32" (lpSystemInfo As SYSTEM_INFO)
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Private Declare Function ExitWindowsEx Lib "user32" (ByVal uFlags As Long, ByVal dwReserved As Long) As Long
Private Declare Function GetCurrentProcess Lib "kernel32" () As Long
Private Declare Function OpenProcessToken Lib "advapi32.dll" (ByVal ProcessHandle As Long, ByVal DesiredAccess As Long, TokenHandle As Long) As Long
Private Declare Function LookupPrivilegeValue Lib "advapi32.dll" Alias "LookupPrivilegeValueA" (ByVal lpSystemName As String, ByVal lpName As String, lpLuid As LARGE_INTEGER) As Long
Private Declare Function AdjustTokenPrivileges Lib "advapi32.dll" (ByVal TokenHandle As Long, ByVal DisableAllPrivileges As Long, NewState As TOKEN_PRIVILEGES, ByVal BufferLength As Long, PreviousState As TOKEN_PRIVILEGES, ReturnLength As Long) As Long

'Private Const ProgramId As Long = 20389
Private Const ProgramId As Long = 853
Private ClaseRutinas As New cls_Rutinas

Enum WinFlagsReestart
    Ewx_Shutdown = 1
    EWX_FORCE = 4
    EWX_LOGOFF = 0
    EWX_REBOOT = 2
End Enum

Private Type SYSTEM_INFO
        dwOemID As Long
        dwPageSize As Long
        lpMinimumApplicationAddress As Long
        lpMaximumApplicationAddress As Long
        dwActiveProcessorMask As Long
        dwNumberOrfProcessors As Long
        dwProcessorType As Long
        dwAllocationGranularity As Long
        dwReserved As Long
End Type

Enum TypeFormat
    FormatString = 0
    FormatDouble = 1
End Enum

Public Function CrearAdoCn() As Object
    Set CrearAdoCn = CreateObject("ADODB.connection")
End Function

Public Function CrearAdoRs() As Object
    Set CrearAdoRs = CreateObject("ADODB.recordset")
End Function

'Public Function Mensajes(ByVal CadMen As String, Optional ByVal BotonCancelar As Boolean = False) As Boolean
'    Dim cMensajes As clsMensajes
'
'    On Error GoTo ErrorMensaje
'    Set cMensajes = New clsMensajes
'    Mensajes = cMensajes.MostrarMensaje(CadMen, BotonCancelar)
'    retorno = cMensajes.Respuesta
'    Set cMensajes = Nothing
'    Exit Function
'ErrorMensaje:
'    retorno = False
'End Function

Public Function AbrirConexion(Optional ByRef Conexion As Object, Optional CnStr As String = "", Optional ByRef DestroyCn As Boolean = False) As ErrObject
    On Error GoTo Falla_Local
    If IsMissing(Conexion) Or Conexion Is Nothing And Trim(CnStr) <> "" Then
        Set Conexion = CrearAdoCn()
        Conexion.ConnectionString = CnStr
        Conexion.Open
        DestroyCn = DestroyCn Or True
    Else
        If Conexion.State = adStateClosed Then
            Conexion.Open
        End If
    End If
    Set AbrirConexion = Err
    DestroyCn = DestroyCn Or False
    Exit Function
Falla_Local:
    Set Conexion = Nothing
    Set AbrirConexion = Err
    Exit Function
End Function

Public Function NombreTemp() As String
    
    Dim Valor As Long, TempDir As String
    
    Const MultRnd As Long = 999999999
    
    Randomize
    TempDir = IIf(Right(Environ("temp"), 1) = "\", Environ("temp"), Environ("temp") & "\")
    Valor = Rnd * MultRnd
    
    NombreTemp = TempDir & Format(Valor, "000000000") & ".TMP"
    
    While Dir(NombreTemp) <> ""
        Valor = Rnd * MultRnd
        NombreTemp = TempDir & Format(Valor, "000000000") & ".TMP"
    Wend
    
End Function

Public Sub LlamarTeclado(ByRef ObjText As Object)
    
    Dim Teclado As Object
    
    Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
    
    Call Teclado.ShowKeyboard(ObjText)
    
    Set Teclado = Nothing
    
End Sub

Public Sub LlamarTecladoNumerico(ByRef ValorDef As Double, Decimales As Integer, Optional Titulo As String = "")
    
    Dim Teclado As Object
    
    Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
    
    Call Teclado.ShowNumPad(ValorDef, Decimales) 'Call Teclado.ShowNumPad(ValorDef, 2, Titulo)
    
    Set Teclado = Nothing
    
End Sub

Public Function DesEncriptar(Texto As String) As String
    Dim Rutinas As Object
    Set Rutinas = CreateObject("DLLRutinas.cls_rutinas")
    DesEncriptar = Rutinas.DesEncriptarTexto(Texto)
    Set Rutinas = Nothing
End Function

Public Function Encriptar(Texto As String) As String
    Dim Rutinas As Object
    Set Rutinas = CreateObject("DLLRutinas.cls_rutinas")
    Encriptar = Rutinas.EncriptarTexto(Texto)
    Set Rutinas = Nothing
End Function

Public Function Consecutivos(pConexion As Object, Campo As String, Incrementar As Boolean) As String
    Dim Correlativos As Object
    On Error GoTo Falla_Local
    Set Correlativos = CreateObject("recsun.cls_datos")
    Consecutivos = Correlativos.NO_Consecutivo(ProgramId, pConexion, Campo, Incrementar)
    Set Correlativos = Nothing
    If Trim(Consecutivos) = "" Then GoTo Falla_Local
    Exit Function
Falla_Local:
    'Mensajes "Ocurri� un error con los consecutivos. Windows se reiniciar�", False
    'Mensajes LoadResString(187)
    Call ReiniciarWindows(EWX_REBOOT)
End Function

Public Sub SendKeys(Caracteres As String, Optional Espera As Boolean = False, Optional Repetir As Boolean = False)
    
    Dim Teclas As Object
    
    Set Teclas = CreateObject("DLLKeyboard.DLLTeclado")
    
    Teclas.SendKeys Caracteres, Espera, Repetir
    
    Set Teclas = Nothing
    
End Sub

Public Function TruncarCampo(Rs As Object, Campo As String, Valor As Variant, Optional Truncar As Boolean = False) As Variant
    Dim Campos As Object
    Set Campos = CreateObject("DLLRutinas.cls_rutinas")
    TruncarCampo = cls_Rutinas.ValCampo(Rs, Campo, Valor, Truncar)
End Function

Public Sub CopiarRegistro(RsOrigen As Object, ByRef RsDestino As Object)
    
On Error GoTo Falla_Local
    FileGen = NombreTemp                                            'SOLITAMOS UN NOMBRE DE ARCHIVO TEMPORAL
    
    RsOrigen.Save FileGen, adPersistADTG                             'LO GRABAMOS
    Set RsDestino = CrearAdoRs
    RsDestino.Open FileGen                                          'ABRIMOS NUEVO REGISTRO PARA REFERENCIA
    Kill FileGen                                                    'ELIMINAMOS EL ARCHIVO TEMPORAL
    Exit Sub
Falla_Local:
End Sub

Public Function CalcularAncho(ByVal Cadena As String, ByVal Forma As Object) As Long
    Dim ActualScala As Integer
    ActualScala = Forma.ScaleMode
    Forma.ScaleMode = vbTwips
    CalcularAncho = Forma.TextWidth(Cadena) * 1.1
    Forma.ScaleMode = ActualScala
End Function

Public Function SDecimal() As String
 Const ValorConocido As Double = 123.123
 SDecimal = Mid(CStr(ValorConocido), 4, 1)
End Function

Public Function SMil() As String
 Const ValorConocido As Double = 2123
 SMil = Mid(CStr(FormatNumber(ValorConocido)), 2, 1)
End Function

Public Function EstatusTecla(sTecla As Integer) As Boolean
    
    Dim Teclas As Object
    
    Set Teclas = CreateObject("DLLKeyboard.DLLTeclado")
    
    EstatusTecla = Teclas.EstadoTeclas(sTecla)
    
    Set Teclas = Nothing
    
End Function

Public Function AutomataCheckCad(Cadena As String, SimboloDecimal As String, SimboloMil As String, ByRef PosInvalida As Integer, Optional AceptaPorcentaje As Boolean = False) As Boolean
    Dim Estados(1 To 4) As Boolean, EstadoAct As Integer, Car As String, Pos As Integer
    Estados(1) = False
    Estados(2) = True
    Estados(3) = True
    Estados(4) = False
    EstadoAct = 1
    For Pos = 1 To Len(Cadena)
        Car = Mid(Cadena, Pos, 1)
        Select Case Car
            Case "0" To "9"
                Select Case EstadoAct
                    Case 1
                        EstadoAct = 2
                    Case 2
                        EstadoAct = 2
                    Case 3
                        EstadoAct = 3
                    Case 4
                        EstadoAct = 4
                        Exit For
                End Select
            Case SimboloDecimal
                Select Case EstadoAct
                    Case 1
                        EstadoAct = 4
                        Exit For
                    Case 2
                        EstadoAct = 3
                    Case 3
                        EstadoAct = 4
                        Exit For
                    Case 4
                        EstadoAct = 4
                        Exit For
                End Select
            Case SimboloMil
                Select Case EstadoAct
                    Case 1
                        EstadoAct = 2
                    Case 2
                        EstadoAct = 2
                    Case 3
                        EstadoAct = 3
                    Case 4
                        EstadoAct = 4
                        Exit For
                End Select
            Case "%"
                If AceptaPorcentaje Then
                    Select Case EstadoAct
                        Case 1
                            EstadoAct = 2
                        Case 2
                            EstadoAct = 2
                        Case 3
                            EstadoAct = 3
                        Case 4
                            EstadoAct = 4
                            Exit For
                    End Select
                Else
                    EstadoAct = 4
                End If
            Case Else
                EstadoAct = 4
                Exit For
        End Select
    Next Pos
    If Not Estados(EstadoAct) Then
        PosInvalida = Pos
    End If
    AutomataCheckCad = Estados(EstadoAct)
End Function

Function FormatearValor(Valor As Variant, Decimales As Integer, Optional TFormat As TypeFormat = FormatDouble) As Variant
    If TFormat = FormatDouble Then
        FormatearValor = CDbl(FormatNumber(Valor, Decimales))
    Else
        FormatearValor = FormatNumber(CDbl(Valor), Decimales)
    End If
End Function

Public Sub Apertura_Recordset(ByRef Rec As ADODB.Recordset, Optional LCursor As ADODB.CursorLocationEnum = adUseClient)
    If Rec Is Nothing Then Set Rec = ClaseRutinas.CrearAdoRs
    If Rec.State = adStateOpen Then Rec.Close
    Rec.CursorLocation = LCursor
End Sub

Public Sub SeguirText(ObjTxt As Object, Bteclado As Object, Optional Separacion As Long = 10)
    Bteclado.Left = ObjTxt.Left + ObjTxt.Width + Separacion + 200
    Bteclado.Top = ObjTxt.Top - 40
End Sub

Public Function NombreDelComputador() As String
    Dim Buffers As String * 255, Tamano As Long
    Tamano = GetComputerName(Buffers, 255)
    NombreDelComputador = Replace(Buffers, Chr(0), "")
End Function

Public Function SerialDelProcesador() As Long
    Dim Registro As SYSTEM_INFO
    Call GetSystemInfo(Registro)
    SerialDelProcesador = Registro.dwReserved
End Function

Public Sub ReiniciarWindows(uFlags As WinFlagsReestart, Optional Dormir As Integer = 15, Optional Forzar As Boolean = False)
    ' Se coloco aqui por que manda a reiniciar por todos lados
    ' Si es servidor no deberia reiniciar ni cerrar sesion
    If Pos.EventoDeCierre = 2 And Not Forzar Then
        End
    Else
        Call DormirSistema(Dormir)
        Call Adjusttoken
        Call ExitWindowsEx(uFlags, &HFFFF)
    End If
End Sub

'Initial Catalog=

Public Function ExtraerVariable(CadenaExtraer As String, CadenaOrigen As String, Optional SimboloSeparador As String = "=") As String
    Dim PosIni As Integer, TempCad As String
    ExtraerVariable = ""
    PosIni = InStr(1, CadenaOrigen, CadenaExtraer)
    If PosIni > 0 Then
        TempCad = Mid(CadenaOrigen, PosIni)
        ExtraerVariable = Mid(TempCad, InStr(1, TempCad, "=") + 1, InStr(1, TempCad, ";") - InStr(1, TempCad, "=") - 1)
    End If
End Function

Public Sub Cerrar_Recordset(ByRef Rec As ADODB.Recordset)
    If Rec Is Nothing Then Set Rec = ClaseRutinas.CrearAdoRs
    If Rec.State <> adStateClosed Then Rec.Close
End Sub

'A�ADIDOS DE RECURSOS_ESTELLARV2
Sub MSGridAsign(ByRef GridObj As Object, fila As Integer, Columna As Integer, Texto As Variant, Optional Tamano As Long = -1, Optional Alinear As Alienacion, Optional Formato As String = "")
    GridObj.Row = fila
    GridObj.Col = Columna
    If Tamano >= 0 Then
        GridObj.ColWidth(GridObj.ColSel) = Tamano
    End If
    If Alinear > -1 Then
        GridObj.CellAlignment = Alinear
    End If
    If Formato <> "" Then
        Texto = Format(Texto, Formato)
    End If
    GridObj.Text = IIf(IsNull(Texto), "", Texto)
End Sub

Sub SetDefMSGrid(ByRef GridObj As Object, fila As Integer, Columna As Integer)
    GridObj.Col = Columna
    GridObj.Row = fila
End Sub

Function MSGridRecover(ByRef GridObj As Object, ByVal fila As Integer, ByVal Columna As Integer) As Variant
    Dim Tcol As Integer, Trow As Integer
    Tcol = GridObj.Col
    Trow = GridObj.Row
    GridObj.Row = fila
    GridObj.Col = Columna
    MSGridRecover = GridObj.Text
    SetDefMSGrid GridObj, Trow, Tcol
End Function

Sub FillGridRS(ByRef GridObj As Object, ByVal Registros As ADODB.Recordset, ColumnsAlign() As Integer, Formatos() As String)
    GridObj.Rows = 1
    If Not Registros.EOF Then
        Registros.MoveFirst
        While Not Registros.EOF
            GridObj.Rows = GridObj.Rows + 1
            GridObj.Row = GridObj.Rows - 1
            For Cont = 0 To GridObj.Cols - 1
                If UBound(ColumnsAlign) = 0 Then
                    If UBound(Formatos) = 0 Then
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).Value
                    Else
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).Value, , , (Formatos(Cont))
                    End If
                Else
                    If UBound(Formatos) = 0 Then
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).Value, , (ColumnsAlign(Cont))
                    Else
                        MSGridAsign GridObj, GridObj.Row, (Cont), Registros.Fields(Cont).Value, , (ColumnsAlign(Cont)), (Formatos(Cont))
                    End If
                End If
            Next Cont
            Registros.MoveNext
        Wend
        GridObj.Rows = GridObj.Rows + 1
    End If
End Sub

Sub DelFilaGrid(GridObj As Object, ByVal Linea As Integer)
    Dim fila As Integer, FilaT As Integer, Columna As Integer, ColumnaT As Integer
    For fila = Linea To GridObj.Rows - 2
        For Columna = 1 To GridObj.Cols - 1
            MSGridAsign GridObj, fila, Columna, MSGridRecover(GridObj, fila + 1, Columna)
        Next Columna
    Next fila
    If Linea < GridObj.Rows - 1 Then
        GridObj.Rows = GridObj.Rows - 1
    End If
    SetDefMSGrid GridObj, 1, 1
End Sub

Public Function ContarCaracter(Caracter As String, Cadena As String, Optional UpperCase As Boolean = False) As Long
    Dim LCont As Long, Desplazar As Long
    LCont = 0
    If UpperCase Then
        Caracter = UCase(Caracter)
    End If
    For Desplazar = 1 To Len(Cadena) Step Len(Caracter)
        If Caracter = Mid(Cadena, Desplazar, Len(Caracter)) Then
            LCont = LCont + 1
        End If
    Next Desplazar
    ContarCaracter = LCont
End Function

Public Function ExisteEnCombo(LCombo As ComboBox, Cadena As String) As Boolean
    Dim LCont As Integer, ActualIndex As Integer
    ExisteEnCombo = False
    ActualIndex = LCombo.ListIndex
    For LCont = 0 To LCombo.ListCount - 1
        LCombo.ListIndex = LCont
        If UCase(LCombo.Text) = UCase(Cadena) Then
            ExisteEnCombo = True
            Exit For
        End If
    Next LCont
    LCombo.ListIndex = ActualIndex
End Function

Public Function ConfigurarImpresora() As Boolean
    On Error GoTo Falla_Local
    ConfigurarImpresora = False
    ConfigurarImpresora = Printer.DeviceName <> ""
    ConfigurarImpresora = True
    Exit Function
Falla_Local:
End Function

Public Function DormirSistema(TiempoSegundos As Integer) As Boolean
    Dim mTiempoEspera As Long
    
    DormirSistema = False
    mTiempoEspera = TiempoSegundos: mTiempoEspera = mTiempoEspera * 1000
    Sleep mTiempoEspera
    DormirSistema = True
End Function

Public Function ShowEditor(Optional TextoRef As String = "") As String
    Editor.Buffer = TextoRef
    Editor.Show vbModal
    ShowEditor = Editor.Buffer
    Set Editor = Nothing
End Function

Private Sub Adjusttoken()
    
    Const TOKEN_ADJUST_PRIVILEGES = &H20
    Const TOKEN_QUERY = &H8
    Const SE_PRIVILEGE_ENABLED = &H2
    
    Dim hdlProcessHandle As Long
    Dim hdlTokenHandle As Long
    Dim tmpLUID As LARGE_INTEGER
    Dim tkp As TOKEN_PRIVILEGES
    Dim tkpNewButIgnored As TOKEN_PRIVILEGES
    Dim IBufferNeeded As Long
    
    hdlProcessHandle = GetCurrentProcess()
    OpenProcessToken hdlProcessHandle, (TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY), hdlTokenHandle
    
    LookupPrivilegeValue "", "SeShutdownPrivilege", tmpLUID
    tkp.PrivilegeCount = 1
    tkp.TheLuid = tmpLUID
    tkp.Attributes = SE_PRIVILEGE_ENABLED
    AdjustTokenPrivileges hdlTokenHandle, False, tkp, Len(tkpNewButIgnored), tkpNewButIgnored, IBufferNeeded
    
End Sub

Public Function CalcularTiempos(ByVal TiempoTotal As Long, ByVal Intervalo As Long, ByRef Residuo As Long) As Long
    CalcularTiempos = TiempoTotal \ Intervalo
    Residuo = TiempoTotal Mod Intervalo
End Function

Public Function Cronometro(HoraInicial As Date, HoraFinal As Date) As String
    
    Dim TiempoSegundos As Long, Horas As Long, Minutos As Long, Segundos As Long, Residuo As Long
    
    TiempoSegundos = Abs(DateDiff("s", HoraFinal, HoraInicial))
    Horas = CalcularTiempos(TiempoSegundos, 3600, Residuo)
    Minutos = CalcularTiempos(Residuo, 60, Residuo)
    Segundos = Residuo 'CalcularTiempos(Residuo, 60, Residuo)
    Horas = Horas '+ Residuo
    Cronometro = Format(Horas, "00") & ":" & Format(Minutos, "00") & ":" & Format(Segundos, "00")
    
End Function

Public Function BuscarReglasComerciales(Campo As String, ByRef Conex As Object) As Variant
    Dim Reg As New ADODB.Recordset
    On Error GoTo Falla_Local
    Reg.Open "Select " & Campo & " FROM REGLAS_COMERCIALES", Conex, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not Reg.EOF Then
        BuscarReglasComerciales = Reg.Fields(Campo).Value
        'Reg.Save "C:\REGLASCOMERCIALES.XML", adPersistXML
    End If
    If Reg.State = adStateOpen Then Reg.Close
Falla_Local:
End Function

Public Function DesCorche(Cadena As String, CaracterStop As String) As String
    If Right(Cadena, 1) = CaracterStop Or Len(Cadena) = 0 Then Exit Function
    DesCorche = DesCorche(Mid(Cadena, 1, Len(Cadena) - 1), CaracterStop) & Right(Cadena, 1)
End Function

Public Function PuedeObtenerFoco(objeto As Object) As Boolean
    On Error Resume Next
    If Not objeto Is Nothing Then
        PuedeObtenerFoco = objeto.Enabled And objeto.Visible
        PuedeObtenerFoco = PuedeObtenerFoco And (objeto.Parent.Enabled And objeto.Parent.Visible)
    End If
End Function

Public Function RegConsult(PathReg As String) As Variant
    Dim ObjWSH As Object
    On Error GoTo RegError
    Set ObjWSH = CreateObject("WScript.Shell")
    RegConsult = ObjWSH.RegRead(PathReg)
    Exit Function
RegError:
    RegConsult = ""
    Exit Function
End Function

Public Function RegWrite(PathReg As String, Data As Variant) As Boolean
    Dim ObjWSH As Object
    On Error GoTo RegError
    Set ObjWSH = CreateObject("WScript.Shell")
    If UCase(TypeName(Data)) = "STRING" Then
        ObjWSH.RegWrite PathReg, Data, "REG_SZ"
    Else
        ObjWSH.RegWrite PathReg, Data, "REG_DWORD"
    End If
    RegWrite = True
    Exit Function
RegError:
    RegWrite = False
    Exit Function
End Function

Public Function RegDelete(PathReg As String) As Boolean
    Dim ObjWSH As Object
    On Error GoTo RegError
    Set ObjWSH = CreateObject("WScript.Shell")
    ObjWSH.RegDelete PathReg
    RegDelete = True
    Exit Function
RegError:
    RegDelete = False
    Exit Function
End Function

Public Sub GuardarCadenaLog(Cadena As String, Optional FileName As String = "CIERRES.LOG")
    Dim fHand As Long
    fHand = FreeFile()
    On Error GoTo Falla_Local
    Open FileName For Append Access Write As fHand
    Print #fHand, Cadena
    Close fHand
    Exit Sub
Falla_Local:
    Reset
End Sub

Public Function MSGridLeer(ByRef GridObj As Object, fila As Integer, Columna As Integer) As String
    
    Dim lCol As Long, lRow As Long
    
    lCol = GridObj.Col
    lRow = GridObj.Row
    
    GridObj.Row = fila
    GridObj.Col = Columna
    MSGridLeer = GridObj.Text
    GridObj.Row = lRow
    GridObj.Col = lCol
    
End Function

Public Sub MostarEdit(ObjText As Object, ObjGrid As Object, Optional DefValor As Variant = "", Optional Activar As Boolean = False)
    ObjText.Top = ObjGrid.Top + ObjGrid.CellTop + 15
    ObjText.Left = ObjGrid.Left + ObjGrid.CellLeft + 15
    ObjText.Width = ObjGrid.CellWidth - 30
    ObjText.Height = ObjGrid.CellHeight - 30
    ObjText.Visible = Activar
    ObjText.Text = DefValor
    ObjGrid.Enabled = Not Activar
    
    If Activar Then
        ObjText.SetFocus
    End If
End Sub

Public Function ExisteCampoTabla(pCn As Object, pCampo As String, Optional pRs, Optional pSql As String = "", Optional pValidarFecha As Boolean = False) As Boolean
    Dim mAux As Variant
    Dim MRS As New ADODB.Recordset
    On Error GoTo Error
    If Not IsMissing(pRs) Then
        mAux = pRs.Fields(pCampo).Value
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        'If IsMissing(pCn) Then
            'Debug.Print Pos.CnAdm
            'MRS.Open pSql, Pos.CnAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
        'Else
            MRS.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        'End If
        ExisteCampoTabla = True
        MRS.Close
    End If
    Exit Function

Error:
    'MsgBox Err.Description
    Err.Clear
    ExisteCampoTabla = False
End Function

Public Function ExisteTabla(Tabla As String, Conexion As ADODB.Connection) As Boolean
    
    Dim RsBusqueda As New ADODB.Recordset
    
    On Error GoTo Error
    
    SQL = "SELECT TOP 1 * FROM " & Tabla
    
    RsBusqueda.Open SQL, Conexion, adOpenForwardOnly, adLockReadOnly
    
    ExisteTabla = True
    
    RsBusqueda.Close
    
    Exit Function
    
Error:
    
    'Debug.Print Err.Description
    
    ExisteTabla = False
    
End Function

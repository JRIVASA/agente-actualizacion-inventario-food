Attribute VB_Name = "modInvFood"
Type StrucApp
    sArchivo                                                                        As String
    sServidor                                                                       As String
    sUsuario                                                                        As String
    sPassword                                                                       As String
    sBD                                                                             As String
    sConnectTimeOut                                                                 As Long
    sCommandTimeOut                                                                 As Long
    sServidorRemoto                                                                 As String
    sBDRemota                                                                       As String
    
End Type

Global Const gCodProducto = 859
Global Const gNombreProducto = "Stellar BUSINESS"
Global gPK As String

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public mEstrucApp                                                                  As StrucApp

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Global G_BD_ADM As String
Global G_BD_POS As String

Global MantenerInfoCliente  As Boolean
Global CostoMultiMonedaTasaHistorica As Boolean

Global CampoFactorMonedaProd As Boolean
Global CampoCostoRep As Boolean
Global CampoCostoPro As Boolean

Global CodMonPred           As String
Global DesMonPred           As String
Global SimMonPred           As String
Global FacMonPred           As Double
Global DecMonPred           As Integer

Global EstimacionINV        As Variant
Global EstimacionPV         As Variant

Global ExisteTablaHistoricoMonedas      As Boolean
Global ExisteTablaHistoricoCostos       As Boolean

Global ConceptoAlternoVentasFOOD        As String
Global ConceptoAlternoDevolucionesFOOD  As String

Global ConceptoVentasNoFiscales         As String
Global ConceptoDevolucionesNoFiscales   As String
Global DiscriminarDocumentoNoFiscal     As Boolean

Global ManejaIGTF                       As Boolean

Global FOOD_CargoServicio_CodProducto   As String ' Codigo del Producto _
utilizado para el cargo por servicio. Se le asigna el Costo = Precio _
para que no genere utilidad en el an�lisis de venta.

Public Function ObtenerConfiguracion(ByVal sIniFile As String, ByVal sSection As String, _
ByVal SKey As String, ByVal sDefault As String) As String
    Dim STemp As String * 10000
    Dim NLength As Integer
    
    STemp = Space$(10000)
    NLength = GetPrivateProfileString(sSection, SKey, sDefault, STemp, 9999, sIniFile)
    ObtenerConfiguracion = Left$(STemp, NLength)
End Function

Public Sub EscribirConfiguracion(ByVal sIniFile As String, ByVal sSection As String, _
ByVal SKey As String, ByVal sDefault As String)
    WritePrivateProfileString sSection, SKey, sDefault, sIniFile
End Sub

Sub Main()
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    Dim mCls As clsInvFood
    Dim mCnAdm As ADODB.Connection, mCnPos As ADODB.Connection
    
    Dim mEtapa As String
    
    On Error GoTo Errores
    
    If App.PrevInstance Then End
    
    IniciarConfiguracion
    
    Dim NewUser As String, NewPassword As String
    
    NewUser = Empty: NewPassword = Empty
    
    mEtapa = "Estableciendo Conexiones"
    
    If EstablecerConexion(mCnAdm, True, mEstrucApp.sUsuario, _
    mEstrucApp.sPassword, True, NewUser, NewPassword) Then
        
        If NewUser <> Empty Or NewPassword <> Empty Then
            EscribirConfiguracion mEstrucApp.sArchivo, "Server", "User", NewUser
            EscribirConfiguracion mEstrucApp.sArchivo, "Server", "Password", NewPassword
        End If
        
        NewUser = Empty: NewPassword = Empty
        
        If EstablecerConexion(mCnPos, False, mEstrucApp.sUsuario, _
        mEstrucApp.sPassword, True, NewUser, NewPassword) Then
            
            If NewUser <> Empty Or NewPassword <> Empty Then
                EscribirConfiguracion mEstrucApp.sArchivo, "Server", "User", NewUser
                EscribirConfiguracion mEstrucApp.sArchivo, "Server", "Password", NewPassword
            End If
            
            Set mCls = New clsInvFood
            
            mCls.IniciarAgenteInvFood mCnAdm, mCnPos
            
        End If
        
    End If
    
    End
    
Errores:
    
    EscribirLog Err.Number, Err.Description, "Main. " & mEtapa
    
    End
    
End Sub

Private Sub IniciarConfiguracion()
    
    With mEstrucApp
        
        .sArchivo = App.Path & "\Setup.ini"
        
        .sCommandTimeOut = Val(ObtenerConfiguracion(.sArchivo, "Server", "CommandTimeOut", ""))
        .sConnectTimeOut = Val(ObtenerConfiguracion(.sArchivo, "Server", "ConnectionTimeOut", ""))
        
        .sServidor = ObtenerConfiguracion(.sArchivo, "Server", "SRV_LOCAL", "")
        .sBD = ObtenerConfiguracion(.sArchivo, "Server", "BD", "VAD10")
        
        .sServidorRemoto = ObtenerConfiguracion(.sArchivo, "Server", "SRV_REMOTA", "")
        .sBDRemota = ObtenerConfiguracion(.sArchivo, "Server", "BDREMOTA", "VAD20")
        
        G_BD_ADM = .sBD
        G_BD_POS = .sBDRemota
        
        .sUsuario = ObtenerConfiguracion(.sArchivo, "Server", "User", "SA")
        .sPassword = ObtenerConfiguracion(.sArchivo, "Server", "Password", "")
        
        If Not (UCase(.sUsuario) = "SA" And Len(.sPassword) = 0) Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If Not mClsTmp Is Nothing Then
                .sUsuario = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sUsuario)
                .sPassword = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, .sPassword)
            End If
            
        End If
        
        MantenerInfoCliente = Val(ObtenerConfiguracion(.sArchivo, "Entrada", "MantenerInfoCliente", Empty)) = 1
        CostoMultiMonedaTasaHistorica = Val(ObtenerConfiguracion(.sArchivo, "Entrada", "CostoMultiMonedaTasaHistorica", "1")) = 1
        
        DiscriminarDocumentoNoFiscal = Val(ObtenerConfiguracion(.sArchivo, "Entrada", "DiscriminarDocumentoNoFiscal", "0")) = 1
        ConceptoVentasNoFiscales = ObtenerConfiguracion(.sArchivo, "Entrada", "ConceptoVentasNoFiscales", Empty)
        ConceptoDevolucionesNoFiscales = ObtenerConfiguracion(.sArchivo, "Entrada", "ConceptoDevolucionesNoFiscales", Empty)
        
        ConceptoAlternoVentasFOOD = ObtenerConfiguracion(.sArchivo, "Entrada", "ConceptoAlternoVentasFOOD", Empty)
        ConceptoAlternoDevolucionesFOOD = ObtenerConfiguracion(.sArchivo, "Entrada", "ConceptoAlternoDevolucionesFOOD", Empty)
        
    End With
    
End Sub

Public Function EstablecerConexion(ByRef pCn As ADODB.Connection, pAdm As Boolean, _
Optional ByRef pUser, Optional ByRef pPassword, Optional AuthReset As Boolean = False, _
Optional ByRef NewUser As String, Optional ByRef NewPassword As String) As Boolean
    
Retry:
    
    On Error GoTo ErrHandler
    
    Dim mCadena As String
    Dim mServidor As String
    
    Set pCn = New ADODB.Connection
    
    pCn.ConnectionTimeout = mEstrucApp.sConnectTimeOut
    pCn.Open CadenaConexion(mEstrucApp.sServidor, pAdm)
    pCn.CommandTimeout = mEstrucApp.sCommandTimeOut
    
    EstablecerConexion = True
    
    Exit Function
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If AuthReset Then
        
        If Err.Number = -2147217843 Then
            
            Dim mClsTmp As Object
            
            Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
            
            If mClsTmp Is Nothing Then GoTo UnhandledErr
            
            TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
            
            If Not IsEmpty(TmpVar) Then
                pUser = TmpVar(0): pPassword = TmpVar(1)
                NewUser = TmpVar(2): NewPassword = TmpVar(3)
                Resume Retry
            End If
            
            Set mClsTmp = Nothing
            
        End If
        
    End If
    
UnhandledErr:
    
    EscribirLog mErrNumber, mErrDesc, "Conectando con " & IIf(pAdm, mEstrucApp.sBD, mEstrucApp.sBDRemota)
    
End Function

Private Function CadenaConexion(pServidor As String, pAdm As Boolean)
    With mEstrucApp
        CadenaConexion = "Provider=SQLOLEDB.1;User ID=" & .sUsuario & ";Password=" & .sPassword _
        & ";Initial Catalog=" & IIf(pAdm, .sBD, .sBDRemota) _
        & ";Data Source=" & IIf(Trim(pServidor) <> Empty, pServidor, .sServidor) _
        & ";Persist Security Info=True"
    End With
End Function

Public Function Reasignar_SInfo_ConStr(pConStr As String) As String
    
    'Debug.Print pConStr
    
    Dim TempPassword As String
    
    If UCase(pConStr) Like "*PASSWORD=*" Or UCase(pConStr) Like "*PWD=*" Then
    
        'La conexi�n tiene los valores, no deber�a haber problemas al abrirla.
        
        Reasignar_SInfo_ConStr = pConStr
        
    Else
    
        If (UCase(pConStr) Like "*" & UCase(mEstrucApp.sUsuario) & "*") Then
            TempPassword = mEstrucApp.sPassword
        Else
            TempPassword = ""
        End If
    
        Reasignar_SInfo_ConStr = pConStr & IIf(Right(pConStr, 1) <> ";", ";", "") & "Password=" & TempPassword & ";"
    
    End If
    
    'Debug.Print Reasignar_SInfo_ConStr
    
End Function

Public Function Correlativo(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim MRS As ADODB.Recordset
    Dim mSQL As String
    Dim mValor As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * From MA_CORRELATIVOS WHERE (CU_Campo = '" & pCampo & "')"
    
    Set MRS = New ADODB.Recordset
    
    MRS.Open mSQL, pCn, adOpenDynamic, adLockPessimistic, adCmdText
    
    If Not MRS.EOF Then
        
        MRS!NU_VALOR = MRS!NU_VALOR + 1
        mValor = Format(MRS!NU_VALOR, MRS!CU_FORMATO)
        
        MRS.Update
        
    End If
    
    Correlativo = mValor
    
    Exit Function
    
Errores:
    
    EscribirLog Err.Description, Err.Number, "Buscando Correlativos"
    
End Function

Public Sub EscribirLog(ByVal pNumero, ByVal pDescripcion, ByVal pProceso)
    
    Dim mCadenaError As String
    
    Open App.Path & "\AgentInvFood.Log" For Append Access Write As #1
    
    mCadenaError = Now & ",Error: " & pNumero & ", " & pDescripcion & ",Proceso: " & pProceso
    
    Print #1, mCadenaError
    
    Close #1
    
End Sub

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = Empty Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim MRS As ADODB.Recordset: Set MRS = New ADODB.Recordset
    
    MRS.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarValorBD = MRS.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    MRS.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function BuscarValorBD_Strict(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    Dim MRS As ADODB.Recordset: Set MRS = New ADODB.Recordset
    
    MRS.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarValorBD_Strict = MRS.Fields(Campo).Value
    Else
        BuscarValorBD_Strict = pDefault
    End If
    
    MRS.Close
    
    Exit Function
    
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Function FactorMonedaProducto(ByVal pCodigo As String, pCn As Object) As Double
    FactorMonedaProducto = BuscarValorBD("n_Factor", "SELECT * FROM MA_MONEDAS " & _
    "WHERE c_CodMoneda IN (SELECT TOP 1 c_CodMoneda " & _
    "FROM MA_PRODUCTOS WHERE c_Codigo = '" & QuitarComillasSimples(pCodigo) & "')", 1, pCn)
End Function

Public Function ExisteCampoTabla(pCampo As String, _
Optional pRs, _
Optional pSql As String = "", _
Optional pValidarFecha As Boolean = False, Optional pCn) As Boolean
    
    Dim mAux As Variant
    Dim MRS As New ADODB.Recordset
    
    On Error GoTo Error
    
    If Not IsMissing(pRs) Then
        mAux = pRs.Fields(pCampo).Value
        
        If pValidarFecha Then
            ExisteCampoTabla = mAux <> "01/01/1900"
        Else
            ExisteCampoTabla = True
        End If
    Else
        'If IsMissing(pCn) Then
            'mRs.Open pSql, ENT.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
        'Else
            MRS.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
        'End If
        
        ExisteCampoTabla = True
        MRS.Close
    End If
    
    Exit Function
    
Error:
    
    'MsgBox Err.Description
    'Debug.Print Err.Description
    
    Err.Clear
    ExisteCampoTabla = False
    
End Function

Public Function CrearCampoTabla(ByVal pTabla As String, ByVal pCampo As String, _
ByVal pTipoDato As String, pCn As ADODB.Connection, _
Optional ByVal pValorDefault As String = Empty, _
Optional ByVal pPermiteNULL As Boolean = False, _
Optional ByVal pIdDefault As String = "*") As Boolean
    
    On Error Resume Next
    
    Dim mBD As String: mBD = IIf(IsEmpty(pBD), Empty, pBD & ".")
    
    Dim SQL As String: SQL = _
    "IF NOT EXISTS(SELECT * FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & pTabla & "' AND COLUMN_NAME = '" & pCampo & "')" & vbNewLine & _
    "BEGIN" & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " ADD " & pCampo & " " & pTipoDato & " " & _
    IIf(pPermiteNULL, "NULL", "NOT NULL") & IIf(pIdDefault = "*", _
    " " & "CONSTRAINT" & " " & pTabla & "_DEFAULT_" & pCampo, IIf(IsEmpty(pIdDefault), Empty, " " & pIdDefault)) & _
    IIf(IsEmpty(pValorDefault), Empty, " " & "DEFAULT" & " " & pValorDefault) & vbNewLine & _
    "   ALTER TABLE dbo." & pTabla & " " & "SET (LOCK_ESCALATION = TABLE)" & vbNewLine & _
    "   PRINT 'TABLA ACTUALIZADA'" & vbNewLine & _
    "END"
    
    pCn.Execute SQL, Success
    
    If Err.Number = 0 Then CrearCampoTabla = True
    
End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "", _
Optional pControlarError As Boolean = True) As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    If pControlarError Then On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function EndOfDay(Optional ByVal pFecha) As Date
    If IsMissing(pFecha) Then pFecha = Now
    pFecha = SDate(CDate(pFecha))
    EndOfDay = DateAdd("h", 23, DateAdd("n", 59, DateAdd("s", 59, pFecha)))
End Function

Public Function ExecuteSafeSQL(ByVal SQL As String, _
pCn As ADODB.Connection, _
Optional ByRef Records = 0, _
Optional ByVal ReturnsResultSet As Boolean = False, _
Optional ByVal DisconnectedRS As Boolean = False) As ADODB.Recordset
    
    On Error GoTo Error
    
    If ReturnsResultSet Then
        If DisconnectedRS Then
            Set ExecuteSafeSQL = New ADODB.Recordset
            ExecuteSafeSQL.CursorLocation = adUseClient
            ExecuteSafeSQL.Open SQL, pCn, adOpenStatic, adLockReadOnly
            Set ExecuteSafeSQL.ActiveConnection = Nothing
        Else
            Set ExecuteSafeSQL = pCn.Execute(SQL, Records)
        End If
    Else
        pCn.Execute SQL, Records
        Set ExecuteSafeSQL = Nothing
    End If
    
    Exit Function
    
Error:
    
    Set ExecuteSafeSQL = Nothing
    Records = -1
    
End Function

' Formatea Fecha Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

' Formatea Hora Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GTime(ByVal pDate) As String: On Error Resume Next: GTime = FormatDateTime(pDate, vbLongTime): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function STime(ByVal pDate) As String: On Error Resume Next: STime = FormatDateTime(pDate, vbShortTime): End Function


' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

Public Function BuscarReglaNegocioStr(CnAdm As ADODB.Connection, ByVal pCampo, _
Optional ByVal pDefault As String) As String
    
    Dim MRS As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = '" & pCampo & "' "
    
    MRS.Open mSQL, CnAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not MRS.EOF Then
        BuscarReglaNegocioStr = CStr(MRS!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
    BuscarReglaNegocioStr = pDefault
    
End Function
